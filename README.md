## Directions to The Mill at Gortaneden

Please print or save these directions so that you have access to the document while driving.

You are going to need them!

### Directions

* From Castlemaine, Co Kerry get onto the Road to Dingle (R561 going West).  Castlemaine is on the N-70 between Killorglin and Tralee.

* Drive through Boolteens village; this is a small village, two bars and a RC church (all on the right hand side of the road).

* After Boolteen's village look for the GAA Club building (sports field) on the left hand side of the road.  The club is a large building and a yellow wall facing the road on the left hand side.

* 200 meters past the GAA building look for a right hand turn onto a very narrow road, turn right.  At the junction you will see a derelict cottage on the left and a post & rail fence on the right.

* Follow the narrow road for 1 km, as you go down the road keep turning to the right until you get to a bridge and the end of the sealed road.

* Turn right into the gateway (red sandstone walls on both sides of a plum colored wood gate) and you have arrived!


* If you have passed the GAA building and you come to a Cross Road you have gone too far!

* If you miss the second right hand turn on the narrow road you will find yourself at a 'T Junction' at the top of the mountain, you need to turn around!

### Distance
| Km | Text |
| ---: | :--- |
| 0.0 | Castlemaine,
| 3.6 | Boolteens Village, Pub and Church on the Right Hand Side of the road
| 5.3 | GAA Club/Yellow wall on the Left Hand Side of the road
| 5.4 | Turn right off the main road (R561)
| 5.9 | Corner, Follow the road around to the Right
| 6.2 | Turn right and go down the hill
| 6.4 | The Mill at Gortaneden, you have arrived, look for a gate to your right!

| A | B |
| ---: | :--- |
| GPS | 52.1793623, -9.7743535
| [Google Maps](https://goo.gl/maps/Agw8F8mEEnA2) | 52.1793623,-9.7743535 (Degrees.Decimal)
| DMS | North : 52�10'46"<BR>West : 9�46'28"
| [Irish Grid Reference](http://irish.gridreferencefinder.com/?gr=Q7869704565) | Q 78697 04565

Geo Location;

* GPS : 52.179835, -9.774461
* Google Maps 52.1793623,-9.7743535) Degrees.Decimal
* DMS 52 10 46 (North) -9 46 28 (West) Degrees Minutes Seconds
* Irish Grid Reference Q7869704565

The Mill at Gortaneden on;

* [Air B 'n B](https://www.airbnb.ie/rooms/12146525)
* [Facebook](https://www.facebook.com/TheMillatGortaneden/)
* [Google](https://plus.google.com/100389193973255186436)
